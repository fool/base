Prints out a number in a few different bases. Examples:

$ base 10 32
binary  100000
three   1012
octal   40
decimal 32
hex     20


$ base 16 ffa
binary  111111111010
three   12121111
octal   7772
decimal 4090
hex     ffa


$ base 2 010101010101
binary  10101010101
three   1212120
octal   2525
decimal 1365
hex     555

