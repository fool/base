#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/**
 * Prints a number in a few different bases.
 * Jordan Sterling, 2007
 *
 *
 * Copyright 2007 Jordan Sterling
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

void printBase(int, unsigned long, const char *);
int main(int argc, char **argv)
{
    if(argc != 3)
    {
        printf("Bad. It goes base <input base> <input number>\n");
        return 1;
    }
    char *inputBase = argv[1];
    char *number = argv[2];

    int base = atoi(inputBase);
    unsigned long x = strtoul(number, 0, base);
    
    printBase(2, x, "binary");
    printBase(3, x, "three");
    printf("octal\t%lo \n", x); 
    printf("decimal\t%lu \n", x);
    printf("hex\t%lx \n", x);
    return 0;
}
void printBase(int base, unsigned long l, const char *name)
{
    int i = 0;
    unsigned long *holder = (unsigned long*)calloc(100, sizeof(unsigned long));
    while(l > 0)
    {
        holder[i++] = l % base;
	l /= base;
    }
    printf("%s\t", name);
    while(--i > -1)
        printf("%lu", holder[i]);
    printf("\n");
    free(holder);
}

